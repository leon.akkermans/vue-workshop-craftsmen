import api from './api';

export async function getBookings() {
  return api.get('/bookings');
}

export async function postBooking(payload) {
  return api.post('/bookings', payload);
}