# vue-workshop-craftsmen

## Systeemvereisten:

- Node.js, bij voorkeur >= 12.18

## Opstarten:

1. dependencies installeren: `npm install`
2. frontent server starten: `npm run serve`
3. in extra terminal tab, de API server starten: `npm run serve-api`

De frontend zal te bekijken zijn op: http://localhost:8080/ 

## API endpoints

In `src/services/booking-api.js` zijn utilities klaargezet voor het aanroepen
van de APIs:

- Get bookings: geeft een array van boekingen
- Post booking: accepteert een boeking object dat tevens wordt gevalideert
  (zie `api/server.js` voor de modeldefinities). Wanneer de validatie slaagt
  wordt de boeking opgeslagen, deze zal bij de eerst volgende call naar 
  'get bookings' in de lijst staan.
  
Wanneer je tijdens het ontwikkelen de opgeslagen data wilt resetten, kun je 
dat doen door de server te herstarten.

## Inbegrepen libraries

Vue addons:

- [Bootstrap](https://getbootstrap.com/) / [Bootstrap-Vue](https://bootstrap-vue.org/)
- [Portal-Vue](https://portal-vue.linusb.org/) (de voorganger van de nieuwe Vue 3 feature 'Teleport')
- [Vuelidate](https://vuelidate.js.org/) voor formuliervalidaties

Generieke libraries:

- [Axios](https://www.npmjs.com/package/axios) http client
- [Case](https://www.npmjs.com/package/case) voor het converteren van bijv `snake_case` naar `Sentence case`
- [Lodash](https://lodash.com/) voor utilities zoals debounce

## Oefeningen

### 1. Bookings overview:

- Haal met een http request de boekingen op.
- Maak gebruik van `v-if` en `v-for` en evt bootstrap componenten
om de items te renderen.

### 2. Booking form basisopzet:

- Voeg op stap 1 een tekstveld toe voor `client_reference`.
- Voeg op de laatste stap de post call toe waarmee de boeking 
opgeslagen wordt.
- De API zal een error teruggeven over ontbrekende verplichte velden,
omdat we alleen nog een client reference opsturen. Voeg een
bootstrap alert component toe waarin de validatiemelding getoond wordt. 
- Probeer een constructie te creëren waarbij het bovenliggende booking
formulier continu een volledig up-to-date versie van het gehele booking
object heeft. Het gebruik van `v-model` zal in deze situatie onvoldoende
blijken omdat daarmee de data niet correct naar boven bubbelt.
Zorg er dmv data bindings en event emitters voor dat het bovenliggende 
booking object continu correct bijgewerkt wordt.

### 3. Booking form cargo details en consignee

- Voeg alle nodige velden toe om shippers, packages en een consignee
in te kunnen vullen en op te sturen naar de backend.
    
### 4. Oefeningen met compositie

- Reduceer de duplicatie tussen de shipper en consignee blokken
  door er een herbruikbaar component van te maken
- Render de shipper variant in een modal, terwijl die consignee inline
  getoond blijft. Voor de modal kun je voor een bootstrap modal gaan of
  er zelf een bouwen met behulp van Portal-Vue.
- Bouw een adresboek wat gebruikt kan worden bij het invullen van de shipper
  en consignee. Gebruik evt modals en/of portals om de UI op te bouwen.

### 5. Oefeningen met validatie

- Zorg er dmv client side validaties voor dat de server side validatie error
  voorkomen kan worden. Voor de validatie kun je Vuelidate gebruiken, voor de 
  UI heeft boostrap allerlei componenten en classes waarmee je de validity
  van de velden zichtbaar kunt maken.
